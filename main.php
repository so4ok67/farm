<?php
class Animal {
    public $type;
    public $product;
    public $animalId;
    public $typeAnimal = array(0 => array('type' => 'cow', 'product' => 0, 'count' => 0), 1 => array('type' => 'chicken', 'product' => 0, 'count' => 0));

    public function addAnimal($type, $product) {
        foreach ($this->typeAnimal as $animal) {
            if(strcasecmp($type, $animal['type']) == 0) {
                return array('type'=>$type, 'product'=>$product);
            }
        }
        $this->typeAnimal[] = array('type' => $type, 'product'=> 0, 'count' => 0);
        return array('type' => $type, 'product'=>$product);
    }



}



class Farm extends Animal {
    public $livestock = array('id' => '1', 'type' => 'cow', 'product' => '9');
    public $addAnimal;
     function __construct($listAnimals)
    {
        $this->listAnimals = $listAnimals;

    }
    function addAnimal($type, $product)
    {

        $dataAnimal =  parent::addAnimal($type, $product);
        $addAnimalData = array('id'=> count($this->listAnimals), 'type' => $dataAnimal['type'], 'product' => $dataAnimal['product']);
        array_push($this->listAnimals, $addAnimalData);
        return true;
    }

    function getAnimals () {
        for ($i=0; $i < count($this->typeAnimal); $i++) {
            $count = 0;
            foreach ($this->listAnimals as $animal) {
                if(strcasecmp($animal['type'], $this->typeAnimal[$i]['type']) == 0) {
                    $count++;
                }
            }
            $this->typeAnimal[$i]['count'] = $count;
        }
        return $this->typeAnimal;
    }
    function calculateProducts() {
        for ($i=0; $i < count($this->typeAnimal); $i++) {
            foreach ($this->listAnimals as $animal) {
                if(strcasecmp($animal['type'], $this->typeAnimal[$i]['type']) == 0) {
                    $this->typeAnimal[$i]['product'] += (int)$animal['product'];
                }
            }
        }
        return $this->typeAnimal;
    }

}
$animals = [];
for($i=0;$i<10;$i++) {
    array_push($animals, array('id' => $i, 'type' => 'cow', 'product' => rand( 9,  12)));
}
for($j=0;$j<20;$j++) {
    array_push($animals, array('id' => $j + 10, 'type' => 'chicken', 'product' => rand(1,  3)));
}
$farm = new Farm($animals);

$end = true;
while ($end) {
    echo "\nДобавить животное: 1 \n";
    echo "Список животных: 2 \n";
    echo "Рассчитать продукт: 3 \n";
    echo "Выход: 4 \n";
    echo "Выберите из списка действие: ";
    $menu=readline();
    switch ($menu) {
        case 1:

            while (true) {
                echo "Введите тип животного: ";
                $data=readline();
                $typeData = trim( $data,  $characters = " \n\r\t\v\0");
                echo "Введите количество продукта: ";
                $product = readline();
                $productData = (int)$product;
                if(!$typeData || !$productData) {
                    echo "Некорректные данные ввода!\n";
                } else {
                    $farm->addAnimal($typeData, $productData);
                    echo "Животное добавлено!\n";
                    echo "Нажмите любую клавишу для подолжения...";
                    readline();
                    break;
                }
            }
            break;
        case 2:
            $data = $farm->getAnimals();
            foreach ($data as $element) {
                echo $element['type']. " => ". $element['count'] ."\n";
            }

            echo "Нажмите любую клавишу для подолжения...";
            readline();
            break;
        case 3:
            $data = $farm->calculateProducts();
            foreach ($data as $element) {
                echo $element['type']. " => ". $element['product'] ."\n";
            }
            echo "Нажмите любую клавишу для подолжения...";
            readline();
            break;
        case 4:
            $end = false;
            break;
        default;
            echo "Пожалуйста, выберите другое действие ...\n";
            break;
    }
}


